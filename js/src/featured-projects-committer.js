import defaultTemplate from './templates/home/featured-project-committer.mustache';

// Newsroom API
const newsroomApiBaseUrl = 'https://newsroom.eclipse.org/api';

const editionMapper = edition => {
    return {
        title: edition.title,
        id: edition.id,
        type: edition.type,
        featuredCommitter: edition.featured_committer,
        featuredProject: edition.featured_project,
        ads: edition.ads
    };
}


const getEdition = async () => {
    try {
        const response = await fetch(`${newsroomApiBaseUrl}/edition?options[orderby][nid]=DESC&pagesize=1`);
        const data = await response.json();
        if (data.editions.length === 0) throw new Error('No edition found.');

        const edition = editionMapper(data.editions[0]);
        
        return [edition, null];
    } catch (error) {
        console.error(error);

        return [null, error];
    }
}

// Projects API
const projectApiBaseUrl = "https://projects.eclipse.org/api";

const getProject = async projectId => {
    try {
        const response = await fetch(`${projectApiBaseUrl}/projects/${projectId}.json`);
        const data = await response.json();
        if (data.length === 0) throw new Error('No project found.');

        const project = data[0];

        return [project, null];
    } catch (error) {
        console.error(error);

        return [null, error];
    }
}

// eclipsefdnFeaturedProjectCommitter widget
const renderError = message => `<div class="alert alert-danger" role="alert">${message}</div>`;

const render = async () => {
    const element = document.querySelector('.eclipsefdn-featured-project-committer');
    if (!element) return;

    const [{ featuredCommitter, featuredProject }, error] = await getEdition();

    if (error) {
        element.innerHTML = `<div class="alert alert-danger" role="alert">Could not retrieve featured committer.</div>`
        return;
    }

    const [project, projectError] = await getProject(featuredProject.id);

    if (projectError) {
        element.innerHTML = renderError(`Could not retrieve project ${featuredProject.id}`);
        return;
    }

    const featuredProjectDetails = { 
        ...featuredProject, 
        name: project.name, 
        logo: project.logo 
    };

    const data = { committer: featuredCommitter, project: featuredProjectDetails };

    element.innerHTML = defaultTemplate(data);
}

const eclipsefdnFeaturedProjectCommitter = {
    render
}

export default eclipsefdnFeaturedProjectCommitter;
