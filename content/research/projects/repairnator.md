---
title: "Repairnator"
date: 2017-02-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/default-logo.png"
tags: ["Automated Repair"]
homepage: "https://www.eclipse.org/repairnator"
facebook: ""
linkedin: ""
twitter: ""
youtube: "https://youtu.be/sHkogNGyBSs"
funding_bodies: []
eclipse_projects: ["technology.repairnator"]
project_topic: "Tools / OSS"
summary: "Open-source and open-innovation platform for automated program repair between academia and research"
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **Repairnator**

Eclipse Repairnator is a bot for automated program repair. It constantly monitors software bugs discovered during continuous integration (CI) and tries to fix them. If it succeeds in synthesizing a valid patch, Repairnator proposes the patch to the development team as a pull request.


[Repairnator Flyer](https://github.com/eclipse/repairnator)"
---
