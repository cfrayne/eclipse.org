---
title: "AGILE-IoT"
date: 2016-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/agile.png"
tags: ["IoT"]
homepage: "http://agile-iot.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/groups/8496876"
twitter: "https://twitter.com/agile_iot"
youtube: "https://www.youtube.com/channel/UC8JldOp0AeEPCblQjAetirQ"
funding_bodies: ["horizon2020"]
eclipse_projects: ["iot.agail"]
project_topic: IoT
summary: "A language-agnostic, modular software and hardware gateway framework for the Internet of Things."
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **Agile-IoT**

AGILE project aims to create an open, flexible and widely usable IoT solution at disposal of industries (startups, SMEs, tech companies) and individuals (researchers, makers, entrepreneurs) as a framework that consists of:

- A modular IoT gateway enabling various types of devices (wearables, home appliances, sensors, actuators, etc.) to be connected with each other and to the Internet;

- Data management and device control maximizing security and privacy, at local level and in the cloud, technologies and methodologies to better manage data privacy and ownership in the IoT;

- Support of various open and private clouds;

- Recommender and visual developer’s interfaces enabling easy creation of applications to manage connected devices and data;

- Support of mainstream IoT/M2M protocols, and SDKs from different standardization bodies for device discovery and communication;

- Two separate gateway hardware versions: a) the ‘maker’s version, based on the popular RaspberryPi platform for easily prototyping and
  attracting the current community; b) the ‘industrial’ version for more industrial and production-ready applications;

- An ecosystem of IoT applications shareable among users and developers leveraging on existing initiatives by key stakeholders in this domain, like Canonical and Ubuntu Snappy IoT ecosystem.


Piloted in relevant open areas (fields and in a port) for field & cattle monitoring through drones, air quality & pollution monitoring and in smart retail, AGILE will be easily adaptable and usable in different contexts serving as an horizontal technology for fast IoT prototyping and engineering in different domains. Following an open hardware/software approach, harnessing the power of IoT developers and entrepreneurs communities, AGILE aims to offer tools to overcome limitations imposed by closed and vertical walled gardens for IoT apps development, offering a fully open platform for integration and adaptation with 3rd parties enabling a new marketplace for IoT apps


This project was running from January 2016 to December 2018."
---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium

* FBK/Create-net - Italy (coordinator)
* IMEC VZW - Belgium
* Eclipse Foundation Europe GmbH - Germany
* ATOS SPAIN SA - Spain
* CANONICAL GROUP LIMITED - United Kingdom
* Resin.io - United Kingdom
* Jolocom UG - Germany
* UNIVERSITAT PASSAU - Germany
* Sky-Watch - Denmark
* BIOASSIST SA - Greece
* Orange - Belgium
* Startupbootcamp - Spain
* TU Graz - Austria
* INRIA - France
* EUROTECH SPA - Italy
* IoTango S.A. - United States

{{</ grid/div>}}
{{</ grid/div>}}
