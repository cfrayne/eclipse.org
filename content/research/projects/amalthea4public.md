---
title: "Amalthea4public"
date: 2014-10-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/amalthea4public.png"
tags: ["CPS", "Modeling"]
homepage: "http://www.amalthea-project.org/"
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["itea3"]
eclipse_projects: ["automotive.app4mc"]
project_topic: CPS
summary: "A platform for engineering embedded multi- and many-core software systems."
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **Amalthea4public**

The core scope of the Amalthea4public project was to enable efficient and effective software engineering for embedded multi-core systems. The focus was predominantly automotive systems but can be applied in aerospace and other systems design contexts. The goal of AMALTHEA4public was to integrate the results of various publicly funded projects with new developments and use the results of the forerunner AMALTHEA project to foster the transfer into application and to establish a community around the combined and continuous tool chain platform.


The open-source tool chain framework is positioned to be a de-facto standard for future software engineering design flows for automotive and other embedded systems. AMALTHEA’s result was an Eclipse-based open-source tool chain infrastructure with a basic set of tools included. AMALTHEA4public moved ahead with this by adding unique selling points (USPs), showing success stories and integrating recent and new research results. The project added features such as testing, verification and validation, safety, systems engineering, product line engineering and manycore support. It also addresses additional domains like ICT and automation. For current work see Eclipse APP4MC and Eclipse Capra.


This project was running from  September 2014 to August 2017"
---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium

* BHTC GmbH - Germany
* Dortmund University of Applied Sciences and Arts - Germany
* Eclipse Foundation Europe GmbH - Germany
* Fraunhofer IEM - Germany
* Institut for Automation und Kommunication (IFAK) - Germany
* Itemis - Germany
* OFFIS - Germany
* Regensburg University of Applied Science - Germany
* Robert Bosch GmbH - Germany
* Timing Architects Embedded Systems GmbH - Germany
* TWT GmbH Science & Innovation - Germany
* University of Paderborn - Germany
* Carsa - Spain
* CBT - Spain
* Engine Power Components G.E., S.L. - Spain
* Asociación de empresas tecnológicas Innovalia - Spain
* Software Quality Systems S.A. - Spain
* rt-labs AB - Sweden
* University of Gothenburg - Sweden
* AVL Turkey - Turkey

{{</ grid/div>}}
{{</ grid/div>}}
