---
title: True Vendor Neutrality Levels the Playing Field
section_id: vendor-neutrality
weight: 3
quote: |
  We were very surprised and pleased to learn we would be on equal terms with the big vendors, which with the same voice and the same vote as they have on the projects we participate in.
quote_author: |
    Steve Millidge,

    Payara Services, a member of the Jakarta EE Working Group
---

One of the main reasons organizations have historically chosen to form a new open source association is to
maintain control over the way the ecosystem operates, rather than adopt a governance model they can’t change. Unfortunately, the control these organizations seek ends up stifling open collaboration and innovation
opportunities:

- Small and medium-sized organizations don’t have the financial or legal resources to adequately review
  the governance of multiple, uniquely operated open source associations. This tilts governance in favor
  of larger organizations.

- When larger organizations dominate, the ecosystem can’t fully benefit from the innovation and cutting-edge
  skill sets that smaller, more nimble, and entrepreneurial organizations often bring to the table.

Working groups hosted at the Eclipse Foundation follow a charter that clearly defines the collaborative
boundaries for ecosystem members. Every ecosystem member has an equal voice and an equal vote, no matter
the size of their organization, their membership fees, or how many of their staff participate in the
ecosystem.

This level playing field opens the door for organizations of all sizes, and at all stages of development,
to openly innovate and collaborate. Inevitably, the ecosystem output is more advanced and innovative, and
is relevant to a much wider range of organizations to drive broader technology adoption.
