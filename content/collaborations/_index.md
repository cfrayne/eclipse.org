---
title: Industry Collaborations
date: 2022-09-13T10:25:51-04:00
description: Work with your peers in the industry in a vendor-neutral structure to drive shared innovation
categories: []
keywords:
  - working groups
  - interest groups
  - collaboration
slug: ""
aliases: []
toc: false
draft: false
page_css_file: public/css/collaborations-styles.css
header_wrapper_class: header-collaborations header-collaborations-bg-img
hide_page_title: true
hide_sidebar: true
#seo_title: ""
headline: Industry Collaborations
subtitle: Work with your peers in the industry in a vendor-neutral structure to drive shared innovation
links:
    - - href: "#join-collaboration"
      - text: Join a Collaboration
    - - href: "#start-collaboration"
      - text: Start a Collaboration
    - - href: /org/workinggroups/explore.php
      - text: Explore Collaborations
container: container-fluid collaborations-container
layout: single
---

{{< pages/collaborations/collaborating >}}

{{< pages/collaborations/join-collaboration >}}

{{< pages/collaborations/start-new-collaboration >}}