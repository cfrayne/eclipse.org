---
title: "IDE"
date: 2022-07-25T10:00:00-04:00
description: "Eclipse desktop & web IDEs"
seo_title: Eclipse desktop & web IDEs
categories: []
keywords: ["eclipse", "project", "plug-ins", "plugins", "java", "ide", "swt", "refactoring", "free java", "tools", "platform", "open source", "development environment", "development"]
slug: "" 
aliases: []
toc: false
draft: false
hide_sidebar: true
hide_page_title: true
container: container-full
layout: single
---

{{< grid/div class="container-fluid" isMarkdown="false" >}}
  {{< grid/section-container class="highlight background-grey breadcrumbs-offset" containerClass="container padding-top-60 padding-bottom-60" id="sec_wg">}}
    <div>
      <div class="col-md-14 highlight-content">
        <h1>Desktop IDEs</h1>
        <p>
          The <a href="https://eclipseide.org/">Eclipse IDE</a> is famous for our Java Integrated Development Environment
          (IDE), but we have a number of pretty cool IDEs, including our C/C++ IDE,
          JavaScript/TypeScript IDE, PHP IDE, and more.
        </p>
        <p>
          You can easily combine multiple languages support and other features into
          any of our default packages, and the Eclipse Marketplace allows for virtually
          unlimited customization and extension.
        </p>
        <ul class="list-inline">
          <li class="margin-right-10 margin-bottom-10"><a href="https://eclipseide.org/release/"><img src="images/eclipse-logo.png" alt="Eclipse IDE" hspace="8"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="https://theia-ide.org/"><img src="images/theia_logo.svg" alt="Eclipse IDE" width="150"/></a></li>
        </ul>
      </div>
      <div class="col-md-9 right highlight-img hidden-xs hidden-sm col-sm-offset-1">
        <img src="images/desktop-ide-screenshot.jpg" class="img-responsive" alt="Eclipse Desktop IDE"/>
      </div>
    </div>
  {{</ grid/section-container >}}

  {{< grid/section-container class="highlight highlight-left" id="sec_ide">}}
    <div>
      <div class="triangle hidden-sm hidden-xs"></div>
      <div class="col-sm-8 col-md-12 center highlight-img hidden-xs padding-top-60 padding-bottom-60">
        <img src="images/web-ide-screenshot-2.jpg" alt="Eclipse Web IDE"  class="img-responsive"/>
      </div>
      <div class="col-sm-16 col-md-12 highlight-content padding-top-60 padding-bottom-60">
        <h1>Cloud IDEs</h1>
        <p>Develop your software wherever you go. It'll be there, in the cloud, right where you left it.  Use your browser to develop with hosted workspaces or install desktop packaging to experience a modern development environment for Java, JavaScript, CSS, and HTML.</p>
        <ul class="list-inline">
          <li class="margin-right-10 margin-bottom-10"><a href="/orion/"><img src="images/orion-logo78x48.jpg" alt="Open Source Platform For Cloud Based Development"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="/che/"><img src="images/EclipseCheLogo.png" alt="Open Source Platform For Cloud Based Development"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="https://dirigible.io/"><img src="images/dirigible_logo.png" alt="Open Source Cloud Development Toolkit"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="https://theia-ide.org/"><img src="images/theia_logo.svg" alt="Eclipse IDE" width="150"/></a></li>
        </ul>
      </div>
    </div>
  {{</ grid/section-container >}}

  {{< grid/section-container class="highlight background-grey breadcrumbs-offset" id="sec_ide_p">}}
    <div>
      <div class="triangle triangle-white hidden-sm hidden-xs"></div>
      <div class="col-md-14 highlight-content padding-top-60 padding-bottom-60">
        <h1>IDE Platforms</h1>
        <p>Create the next generation of developer tooling with our extensible platforms.
          Use your imagination to build services and tools that can be assembled into new IDEs
          or packages tailored to your identity. We provide multiple platforms to build plug-ins
          for desktop tools, distributed services used by cloud IDEs, and browser interfaces.
          You can then publish plug-ins to our Eclipse Marketplace of 1000s.
        </p>
        <ul class="list-inline">
          <li class="margin-right-10 margin-bottom-10"><a href="http://wiki.eclipse.org/Platform"><img width="42" height="42" alt="Eclipse IDE for Java Developers" src="images/java.png"> Eclipse Platform</a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="/orion/"><img src="images/orion-logo-grey78x48.jpg" alt="Open Source Platform For Cloud Based Development"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="/che/"><img src="images/EclipseCheLogo.png" alt="Open Source Platform For Cloud Based Development"/></a></li>
          <li class="margin-right-10 margin-bottom-10"><a href="https://theia-ide.org/"><img src="images/theia_logo.svg" alt="Eclipse IDE" width="150"/></a></li>
        </ul>
      </div>
      <div class="col-md-9 right highlight-img hidden-xs hidden-sm col-sm-offset-1 padding-top-60 padding-bottom-60">
        <img src="images/ide-platform-screenshot.jpg" class="img-responsive" alt="Eclipse IDE Platforms"/>
      </div>
    </div>
  {{</ grid/section-container >}}

  {{< grid/section-container class="highlight highlight-left footer-offset">}}
    <div>
      <div class="triangle hidden-sm hidden-xs"></div>
      <div class="col-md-13 padding-top-60 padding-bottom-60">
        <div class="media">
          <div class="pull-left orange">
            <i class="fa fa-wrench font-size-large"></i>
          </div>
          <div class="media-body">
            <h4 class="media-heading">Tools</h4>
            <p class="emphasis">Extend the extensible platform.</p>
            <p>An impressive collection of tools can be easily installed into your Eclipse desktop IDE, including GUI builders and tools for modeling, charting and reporting, testing, and more.</p>
          </div>
        </div>
        <div class="media">
          <div class="pull-left orange">
            <i class="fa fa-shopping-cart font-size-large"></i>
          </div>
          <div class="media-body">
            <h4 class="media-heading">Marketplace</h4>
            <p class="emphasis">Customize and extend Eclipse and make it your own.</p>
            <p>Use the <a href="/mpc">Eclipse Marketplace Client</a> to find, install, and vote for new plug-ins from our vast ecosystem of providers.</p>
          </div>
        </div>
        <div class="media">
          <div class="pull-left orange">
            <i class="fa fa-gears font-size-large"></i>
          </div>
          <div class="media-body">
            <h4 class="media-heading">Extend</h4>
            <p class="emphasis">Join the Community.</p>
            <p>Extend the desktop and web-based IDEs by writing your own plug-ins using the Plug-in Development Environment (PDE), or mash up features to build the web IDE of your dreams.</p>
          </div>
        </div>
      </div>
      <div class="col-md-8 right highlight-img text-left hidden-xs hidden-sm col-sm-offset-3 padding-top-60">
        <div>
          <h2>Discover</h2>
          <p class="orange"><strong>Find an Eclipse open source project.</strong></p>
          <form id="form-discover-search" class="form-inline form-search-projects input-group custom-search-form" role="form" action="https://projects.eclipse.org/list-of-projects">
            <input id="discover-search-box" class="form-control" type="text" size="25" name="combine" placeholder="Search">
            <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
            <i class="fa fa-search"></i>
            </button>
            </span>
          </form>
          <br/>
          <p><a class="btn btn-info uppercase fw-700" href="//projects.eclipse.org/list-of-projects">List of projects</a></p>
        </div>
      </div>
    </div>
  {{</ grid/section-container >}}
{{</ grid/div >}}
